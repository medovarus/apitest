import { api } from '@/api';

const ReferencesService = {
  /**
   * Получение всех справочников для приложения
   */
  async all() {
    return api.GET('api/v1/references/all');
  },
};

export default ReferencesService;
