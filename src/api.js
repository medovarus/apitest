import axios from 'axios';

class Api {
  axiosInstance;

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: 'http://localhost:8089/',
      timeout: 30000,
      withCredentials: true,
    });
  }

  async GET(path, data) {
    // TODO для примера вставил тот же асинхронный запрос
    const token = await this.axiosInstance.get(
      path,
      {
        data: {},
        params: data || {},
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer',
        },
      },
    );

    const result = await this.axiosInstance.get(
      path,
      {
        data: {},
        params: data || {},
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer',
        },
      },
    );

    console.log('token', token);
    return result.data;
  }
}

export const api = new Api();

export default api;
